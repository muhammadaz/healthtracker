/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package healthtracker;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.time.LocalDate;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

/**
 *
 * @author azhar
 */
public class XML_DOM {

    private File file;
    private User user = new User();
    private Goal goal = new Goal();

    public Goal getGoal() {
        return goal;
    }

    public void setGoal(Goal goal) {
        this.goal = goal;
    }

    public XML_DOM(File fIN) {
        file = fIN;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        file = file;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    /**
     * The method query here takes in username and returns the information about
     * the user from the XML file.
     *
     * @param username
     * @return
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public User getUserInfo(String username) throws ParserConfigurationException, SAXException, IOException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(file);
        document.getDocumentElement().normalize();

        User user = new User();
        NodeList nList = document.getElementsByTagName("user");
        for (int i = 0; i < nList.getLength(); i++) {
            Node nNode = nList.item(i);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                if (eElement.getAttribute("username").equals(username)) {
                    user.setUsername(eElement.getAttribute("username"));
                    user.setName(eElement.getElementsByTagName("name").item(0).getTextContent());
                    user.setSurname(eElement.getElementsByTagName("surname").item(0).getTextContent());
                    user.setAge(Integer.parseInt(eElement.getElementsByTagName("age").item(0).getTextContent()));
                    user.setHeight(Double.parseDouble(eElement.getElementsByTagName("height").item(0).getTextContent()));
                    user.setWeight(Integer.parseInt(eElement.getElementsByTagName("weight").item(0).getTextContent()));
                    user.setPassword(eElement.getElementsByTagName("password").item(0).getTextContent());
                    user.setEmail(eElement.getElementsByTagName("email").item(0).getTextContent());
                }
            }
        }
        return user;
    }

    /**
     * This boolean query checks for the user if it present within the XML file.
     *
     * @param username
     * @return
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public boolean checkUser(String username) throws ParserConfigurationException, SAXException, IOException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(file);
        document.getDocumentElement().normalize();

        boolean exist = false;
        NodeList nList = document.getElementsByTagName("user");
        for (int i = 0; i < nList.getLength(); i++) {
            Node nNode = nList.item(i);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                if (eElement.getAttribute("username").equals(username)) {
                    exist = true;
                }
            }
        }
        return exist;
    }

    /**
     * This boolean query method is used during login to check for the User.
     *
     * @param username
     * @param password
     * @return
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public boolean CheckLogin(String username, String password) throws ParserConfigurationException, SAXException, IOException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(file);
        document.getDocumentElement().normalize();

        boolean exist = false;
        NodeList nList = document.getElementsByTagName("user");
        for (int i = 0; i < nList.getLength(); i++) {
            Node nNode = nList.item(i);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                if (eElement.getAttribute("username").equals(username)) {
                    if ((eElement.getElementsByTagName("password").item(0).getTextContent()).equals(password)) {
                        user.setUsername(eElement.getAttribute("username"));
                        user.setName(eElement.getElementsByTagName("name").item(0).getTextContent());
                        user.setSurname(eElement.getElementsByTagName("surname").item(0).getTextContent());
                        user.setAge(Integer.parseInt(eElement.getElementsByTagName("age").item(0).getTextContent()));
                        user.setHeight(Double.parseDouble(eElement.getElementsByTagName("height").item(0).getTextContent()));
                        user.setWeight(Integer.parseInt(eElement.getElementsByTagName("weight").item(0).getTextContent()));
                        user.setPassword(eElement.getElementsByTagName("password").item(0).getTextContent());
                        user.setEmail(eElement.getElementsByTagName("email").item(0).getTextContent());
                    }
                    exist = true;
                }
            }
        }
        return exist;
    }

    /**
     * The next method query takes weight from the user and enter it into the
     * XML file.
     *
     * @param doc
     * @param username
     * @param newWeight
     * @param file
     * @throws SAXException
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    public void ChangeWeight(Document doc, String username, int newWeight, File file) throws SAXException, IOException, ParserConfigurationException, TransformerException {

        NodeList nList = doc.getElementsByTagName("user");
        for (int i = 0; i < nList.getLength(); i++) {
            Node nNode = nList.item(i);
            System.out.println(nNode.getNodeName());
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                if (eElement.getAttribute("username").equals(username)) {
                    NodeList weight = eElement.getElementsByTagName("weight");
                    Text a = doc.createTextNode(Integer.toString(newWeight));
                    Element p = doc.createElement("weight");
                    p.appendChild(a);

                    weight.item(0).getParentNode().insertBefore(p, weight.item(0));
                }
            }
        }

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(file);
        transformer.transform(source, result);
    }

    /**
     * This method we need to add a new element which will be a new User, this
     * method is used for the registration process.
     *
     * @param doc
     * @param user
     * @return
     * @throws TransformerException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public Element CreateNewUser(Document doc, User user) throws TransformerException, ParserConfigurationException, SAXException, IOException {

        Element el = doc.createElement("user");
        Attr attr = doc.createAttribute("username");
        attr.setValue(user.getUsername());
        el.setAttributeNode(attr);

        el.appendChild(createUserDetail(doc, "name", user.getName()));
        el.appendChild(createUserDetail(doc, "surname", user.getSurname()));
        el.appendChild(createUserDetail(doc, "age", Integer.toString(user.getAge())));
        el.appendChild(createUserDetail(doc, "height", Double.toString(user.getHeight())));
        el.appendChild(createUserDetail(doc, "weight", Integer.toString(user.getWeight())));
        el.appendChild(createUserDetail(doc, "password", user.getPassword()));
        el.appendChild(createUserDetail(doc, "email", user.getEmail()));

        return el;
    }

    /**
     * This method is simple to write the data taken to the XML file.
     *
     * @param doc
     * @param file
     * @throws TransformerException
     * @throws SAXException
     * @throws IOException
     * @throws ParserConfigurationException
     */
    public void writeToXML(Document doc, File file) throws TransformerException, SAXException, IOException, ParserConfigurationException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(file);
        transformer.transform(source, result);
    }

    /**
     * Quick method to create a element which adds a users detail.
     *
     * @param doc
     * @param name
     * @param value
     * @return
     */
    private static Element createUserDetail(Document doc, String name, String value) {
        Element userEl = doc.createElement(name);
        userEl.appendChild(doc.createTextNode(value));
        return userEl;
    }

    /**
     * This method writes creates goal which writes to the XML file as well,
     * using the write XML method.
     *
     * @param document
     * @param userna
     * @param gl
     * @throws TransformerException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public void WriteGoal(Document document, String userna, Goal gl) throws TransformerException, ParserConfigurationException, SAXException, IOException {
        NodeList root = document.getElementsByTagName("user");
        if (checkUser(userna)) {
            for (int i = 0; i < root.getLength(); i++) {
                Node nNode = root.item(i);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    if (eElement.getAttribute("username").equals(userna)) {
                        NodeList emailNode = eElement.getElementsByTagName("email");

                        emailNode.item(0).getParentNode().insertBefore(createGoal(document, gl), emailNode.item(1));

                        writeToXML(document, file);
                    }
                }
            }
        }
    }

    /**
     * This method takes in data of the goal and writes.
     *
     * @param doc
     * @param goal
     * @return
     */
    Element createGoal(Document doc, Goal goal) {

        Element el = doc.createElement("goal");

        Attr attr = doc.createAttribute("goal_id");
        attr.setValue(goal.getGoalid());
        el.setAttributeNode(attr);

        el.appendChild(createUserDetail(doc, "goal_name", goal.getGoalname()));
        el.appendChild(createUserDetail(doc, "goal_type", goal.getGoaltype()));
        el.appendChild(createUserDetail(doc, "goal_weight", Integer.toString(goal.getGoalweight())));
        el.appendChild(createUserDetail(doc, "goal_deadline", String.valueOf(goal.getGoaldeadline())));

        return el;
    }
}
