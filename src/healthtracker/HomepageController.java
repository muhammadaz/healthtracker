/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package healthtracker;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * FXML Controller class
 *
 * @author azhar
 */
public class HomepageController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private Label usernamelabel;

    @FXML
    private Label agelabel;

    @FXML
    private Label heightlabel;

    @FXML
    private Label weightlabel;

    @FXML
    private Label emaillabel;

    @FXML
    private TextField updateweight;

    @FXML
    private TextField goalid;

    @FXML
    private TextField goalname;

    @FXML
    private TextField goaltype;

    @FXML
    private TextField goalweight;

    @FXML
    private TextField goaldate;
    @FXML
    private Label goallabel;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    /**
     * This button handles the update which updates the weight and writes the
     * new weight along with saving the old also.
     *
     * @param event
     * @throws IOException
     * @throws SQLException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    @FXML
    private void HandleUpdateButtonAction(ActionEvent event) throws IOException, SQLException, SAXException, ParserConfigurationException, TransformerException {

        System.out.println("Clicked me!");

        File file = new File("/Users/azhar/HealthTrackerApp/HealthTracker/src/healthtracker/Directory.xml");

        XML_DOM xmlp = new XML_DOM(file);

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(file);
        document.getDocumentElement().normalize();

        xmlp.ChangeWeight(document, username, Integer.parseInt(updateweight.getText()), file);

        weightlabel.setText(updateweight.getText());
    }

    /**
     * This button method handles the logging out of the programe.
     *
     * @param event
     * @throws IOException
     * @throws SQLException
     * @throws TransformerException
     * @throws ParserConfigurationException
     * @throws SAXException
     */
    @FXML
    private void HandleLogoutButtonAction(ActionEvent event) throws IOException, SQLException, TransformerException, ParserConfigurationException, SAXException {

        System.out.println("You clicked me!");
        FXMLLoader loader = new FXMLLoader();
        Parent home_page_parent = loader.load(getClass().getResource("Login.fxml").openStream());

        Scene login_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

        app_stage.hide(); //optional
        app_stage.setScene(login_page_scene);

        app_stage.show();

    }

    String username;

    /**
     * This method attains the user from login page to be used by the Homepage,
     * and sets the user information on the Homepage.
     *
     * @param user
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public void getuser(String user) throws ParserConfigurationException, SAXException, IOException {

        username = user;

        File file = new File("/Users/azhar/HealthTrackerApp/HealthTracker/src/healthtracker/Directory.xml");

        XML_DOM xmlp = new XML_DOM(file);

        xmlp.getUserInfo(user);

        usernamelabel.setText(user);
        weightlabel.setText(Integer.toString(xmlp.getUserInfo(user).getWeight()));
        agelabel.setText(Integer.toString(xmlp.getUserInfo(user).getAge()));
        emaillabel.setText(xmlp.getUserInfo(user).getEmail());
        heightlabel.setText(Double.toString(xmlp.getUserInfo(user).getHeight()));

    }

    /**
     * This button handles the set goal button, which creates a goal and writes,
     * to the XML file.
     *
     * @param event
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws TransformerException
     */
    @FXML
    private void HandleGoalButtonAction(ActionEvent event) throws IOException, ParserConfigurationException, SAXException, TransformerException {

        File file = new File("/Users/azhar/HealthTrackerApp/HealthTracker/src/healthtracker/Directory.xml");
        XML_DOM dom = new XML_DOM(file);

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(file);
        document.getDocumentElement().normalize();

        Pattern p1 = Pattern.compile("[0-9]+");
        Matcher m1 = p1.matcher(goalweight.getText());

        if (!(m1.find() && m1.group().equals(goalweight.getText()))) {
            goallabel.setText("Enter Number");

        }

        
        Goal goal = new Goal();

        goal.setUser(dom.getUserInfo(username));

        goal.setGoalid(goalid.getText());
        goal.setGoalname(goalname.getText());
        goal.setGoaltype(goaltype.getText());
        goal.setGoalweight(Integer.parseInt(goalweight.getText()));
        goal.setGoaldeadline(LocalDate.parse(goaldate.getText()));

        dom.WriteGoal(document, username, goal);

        goallabel.setText("GOAL SET!");
        goalid.clear();
        goalname.clear();
        goaltype.clear();
        goalweight.clear();
        goaldate.clear();

        System.out.println("CLICKED");

    }

}
