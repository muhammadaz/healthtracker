/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package healthtracker;

import java.time.LocalDate;

/**
 *
 * @author azhar
 */
public class Goal {
    
    private User user;
    private String goalname;
    private String goalid;
    private String goaltype;
    private int goalweight;
    private LocalDate goaldeadline;
    private boolean achieved;
    
    public Goal(){

}
    public Goal (User user,String goalname, String goalid, String goaltype, int goalweight, LocalDate deadline) {
        
        this.user = user;
        this.goalname = goalname;
        this.goalid = goalid;
        this.goaltype = goaltype;
        this.goalweight = goalweight;
        this.goaldeadline = deadline;
        this.achieved = false;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getGoalname() {
        return goalname;
    }

    public void setGoalname(String goalname) {
        this.goalname = goalname;
    }

    public String getGoalid() {
        return goalid;
    }

    public void setGoalid(String goalid) {
        this.goalid = goalid;
    }

    public String getGoaltype() {
        return goaltype;
    }

    public void setGoaltype(String goaltype) {
        this.goaltype = goaltype;
    }

    public int getGoalweight() {
        return goalweight;
    }

    public void setGoalweight(int goalweight) {
        this.goalweight = goalweight;
    }

    public LocalDate getGoaldeadline() {
        return goaldeadline;
    }

    public void setGoaldeadline(LocalDate goaldeadline) {
        this.goaldeadline = goaldeadline;
    }


}


