/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package healthtracker;

import com.sun.media.jfxmedia.logging.Logger;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author azhar
 */
public class LoginController implements Initializable {

    private XML_DOM xmlp;
    @FXML
    private Label statuslabel;

    @FXML
    private Label reglabel;

    @FXML
    private TextField username_box;

    @FXML
    private TextField password_box;

    @FXML
    private TextField nametxt;

    @FXML
    private TextField surnametxt;

    @FXML
    private TextField agetxt;

    @FXML
    private TextField heighttxt;

    @FXML
    private TextField weighttxt;

    @FXML
    private TextField username_reg;

    @FXML
    private TextField passtxt;

    @FXML
    private TextField repasstxt;

    @FXML
    private TextField emailtxt;

    /**
     * This method is for Handling the button for login. Which takes to
     * Homepage.
     *
     * @param event
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     */
    @FXML
    private void HandleButtonAction(ActionEvent event) throws IOException, ParserConfigurationException, SAXException {
        System.out.println("You clicked me!");
        FXMLLoader loader = new FXMLLoader();
        Parent home_page_parent = loader.load(getClass().getResource("Homepage.fxml").openStream());
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

        if (isValidLogin()) {

            app_stage.hide(); //optional
            app_stage.setScene(home_page_scene);
            HomepageController hmpagecontroller = (HomepageController) loader.getController();
            hmpagecontroller.getuser(username_box.getText());
            app_stage.show();
        } else {
            username_box.clear();
            password_box.clear();
            statuslabel.setText("Sorry Invalid Credentials.");

        }

    }

    /**
     * This method handles the button for registration. Which one again directs
     * to the Homepage.
     *
     * @param event
     * @throws IOException
     * @throws SQLException
     * @throws TransformerException
     * @throws ParserConfigurationException
     * @throws SAXException
     */
    @FXML
    private void HandleRegiButtonAction(ActionEvent event) throws IOException, SQLException, TransformerException, ParserConfigurationException, SAXException {

        System.out.println("You clicked me!");
        FXMLLoader loader = new FXMLLoader();
        Parent home_page_parent = loader.load(getClass().getResource("Homepage.fxml").openStream());

        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

        if (isRegister()) {

            app_stage.hide(); //optional
            app_stage.setScene(home_page_scene);
            HomepageController hmpagecontroller = (HomepageController) loader.getController();
            hmpagecontroller.getuser(username_reg.getText());
            app_stage.show();
        }

    }

    /**
     * This is a boolean method, which returns true when all of the conditions
     * of the login meet, Searching through the XML file.
     *
     * @return
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    private boolean isValidLogin() throws ParserConfigurationException, SAXException, IOException {

        boolean let_in = false;

        System.out.println("isValidCredentials operation done successfully");
        //return let_in;
        File file = new File("/Users/azhar/HealthTrackerApp/HealthTracker/src/healthtracker/Directory.xml");
        xmlp = new XML_DOM(file);

        if (xmlp.CheckLogin(username_box.getText(), password_box.getText())) {

            let_in = true;
            System.out.println("Correct!");

        }
        return let_in;

    }

    /**
     * This boolean method is for registration, which takes in all of the
     * information which the user inputs and enter into XML file its, whilst
     * also considering the email validation.
     *
     * @return
     * @throws SQLException
     * @throws TransformerException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    private boolean isRegister() throws SQLException, TransformerException, ParserConfigurationException, SAXException, IOException {

        File file = new File("/Users/azhar/HealthTrackerApp/HealthTracker/src/healthtracker/Directory.xml");
        xmlp = new XML_DOM(file);

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(xmlp.getFile());
        doc.getDocumentElement().normalize();

        User user = new User();

        boolean reg_in = false;

        String username = username_reg.getText();
        username = username.toLowerCase();
        String password = passtxt.getText();
        String confPass = repasstxt.getText();

        if (!password.equals(confPass)) {
            reglabel.setText("Passwords not matching!");
            return false;

        }

        Pattern p = Pattern.compile("[a-zA-Z0-9][a-zA-Z0-9._]*@[a-zA-Z0-9]+([.][a-zA-Z]+)+");
        Matcher m = p.matcher(emailtxt.getText());

        if (!(m.find() && m.group().equals(emailtxt.getText()))) {
            reglabel.setText("Email Id Wrong");

            return false;
        }
        Pattern p1 = Pattern.compile("[0-9]+");
        Matcher m1 = p1.matcher(heighttxt.getText());

        if (!(m1.find() && m1.group().equals(heighttxt.getText()))) {
            reglabel.setText("Enter Number");

            return false;
        }

        Matcher m2 = p1.matcher(weighttxt.getText());

        if (!(m2.find() && m2.group().equals(weighttxt.getText()))) {
            reglabel.setText("Enter Number");

            return false;
        }
        Matcher m3 = p1.matcher(agetxt.getText());

        if (!(m3.find() && m3.group().equals(agetxt.getText()))) {
            reglabel.setText("Enter Number");

            return false;
        }

        user.setName(nametxt.getText());
        user.setSurname(surnametxt.getText());
        user.setAge(Integer.parseInt(agetxt.getText()));
        user.setHeight(Integer.parseInt(heighttxt.getText()));
        user.setWeight(Integer.parseInt(weighttxt.getText()));
        user.setUsername(username_reg.getText());
        user.setPassword(repasstxt.getText());
        user.setEmail(emailtxt.getText());
        NodeList nl = doc.getElementsByTagName("root");

        if (!xmlp.checkUser(username_reg.getText())) {

            nl.item(0).appendChild(xmlp.CreateNewUser(doc, user));

            xmlp.writeToXML(doc, file);
            return true;
        } else {
            reglabel.setText("Username Taken");
        }
        return reg_in;

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}
